﻿Imports System.Reflection
Imports System.IO
Imports System.Net
Imports System.Security.Cryptography.X509Certificates

Public Class Form1

    Private FormaCargada As Boolean = False

    Public Function AcceptAllCertifications(ByVal sender As Object,
    ByVal certification As System.Security.Cryptography.X509Certificates.X509Certificate,
    ByVal chain As System.Security.Cryptography.X509Certificates.X509Chain,
    ByVal sslPolicyErrors As System.Net.Security.SslPolicyErrors) As Boolean
        Return True
    End Function

    Private Sub Form1_Activated(sender As Object, e As EventArgs) Handles Me.Activated

        If Not FormaCargada Then

            FormaCargada = True

            '   ACA ESTOS VALORES SON CONSTANTES Y PRIVADOS

            'KeyId = "617F3286-B2C8-45B1-B14C-ED1DACFAB044"
            'PublicKeyId = "9c7dc4a6096e64a3e83ad293fe170576"

            PublicKeyId = mPayPBKey
            KeyId = mPayPVKey

            If My.Application.CommandLineArgs.Count > 0 Then

                ' ACA LEO LOS PARAMETROS NECESARIOS Y LOS METO EN VARIABLES.
                LeeParametros()

                ' ACA SOLO CONSULTARA Y HARA LAS ACTUALIZACIONES QUE HAGAN FALTA. CUANDO PIDAN EN UN FUTURO MODIFICAR EL PROGRAMA, SE HARA POR OTROS RAMALES EN TIPOTRANSACCION 2 Y 3
                If TipoTransaccion = "CONSULTAR Y ACTUALIZAR" Then

                    NumeroPedido = My.Application.CommandLineArgs(1).Trim
                    NotaEntrega = My.Application.CommandLineArgs(2).Trim
                    MontoFinal = My.Application.CommandLineArgs(3).Trim

                    If DebugMode = True Then
                        MsgBox("NumeroPedido: " & NumeroPedido)
                        MsgBox("NotaEntrega: " & NotaEntrega)
                        MsgBox("MontoFinal: " & MontoFinal)
                    End If

                    '   ACA CONSULTO EN LA BD LOS DATOS NECESARIOS PARA TRABAJAR
                    ConsultarDetallePedido(NumeroPedido)

                    '   ACA CONSULTO EL PAGO EN CUESTION Y SE ACTUALIZA EN CUESTION
                    ConsultarActualizar(KeyId, PublicKeyId, Id)

                    '   ACA LEO EL CONTENIDO DEL JSON
                    LeerJSON(DETALLETRANSACCION)

                    '   ACA GUARDO AUDITORIA DEL PROCESO EJECUTADO EN EL AGENTE
                    GuardarRegistroTransaccion()

                End If

            Else
                DesconectarSql1()
            End If

            System.Environment.Exit(0)

        End If

    End Sub

    Private Sub Form1_Load(sender As Object, e As EventArgs) Handles MyBase.Load

        ServicePointManager.Expect100Continue = True
        ServicePointManager.SecurityProtocol = (SecurityProtocolType.Tls12 Or SecurityProtocolType.Tls11 Or SecurityProtocolType.Tls Or SecurityProtocolType.Ssl3)
        ServicePointManager.ServerCertificateValidationCallback = AddressOf AcceptAllCertifications

        gPK = "" & Chr(55) & Chr(81) & Chr(76) & "_" &
        Chr(73) & Chr(55) & Chr(51) & Chr(51) &
        Chr(83) & Chr(78) & Chr(75) & Chr(52) &
        Chr(78) & Chr(50) & Chr(69) & Chr(74) &
        Chr(68) & Chr(49) & Chr(83) & "-" &
        Chr(66) & Chr(44) & Chr(66) & Chr(71) &
        Chr(52) & Chr(44) & Chr(90) & "_" + Chr(71)

        ' ACA DETERMINO SI ESTAMOS EN MODO DEBUG
        If File.Exists(My.Computer.FileSystem.CurrentDirectory & "\Debugmode.ON").ToString.ToUpper Then
            DebugMode = True
        End If

        RecuperacionInicialDeCredenciales()

        ServerInstance = LeerINI(Archivo, "SERVER", "SRV_LOCAL", "")

        ConectarSql1()

        If DebugMode = True Then MsgBox(RUTACONEXIONSQL, MsgBoxStyle.Information, "RutaConexion completa")

        If DebugMode = True Then MsgBox(falla, MsgBoxStyle.Information, "Estatus de Conexion")

        If falla = 0 Then
            ' Esperar Form Activate
        Else
            System.Environment.Exit(0)
        End If

    End Sub

End Class
