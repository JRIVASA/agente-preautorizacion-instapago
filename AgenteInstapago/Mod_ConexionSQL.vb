﻿Imports System.Data.SqlClient
Imports System.Reflection
Imports System.IO

Module Mod_ConexionSql

    Public csSql1 = My.Settings.Item("SqlRuta1")
    Public csSql2 = My.Settings.Item("SqlRuta2")
    Public ConexionSql1 As New SqlConnection
    Public ConexionSql2 As New SqlConnection
    Public cmd As New SqlCommand

    '    Public registrosSql1 As SqlDataReader
    '    Public registrosSql2 As SqlDataReader

    Public Sub RecuperacionInicialDeCredenciales()

        Dim mClsTmp As Object = Nothing

        ' Archivo = My.Computer.FileSystem.CurrentDirectory & "\Setup.ini"
        Archivo = Assembly.GetExecutingAssembly.Location.Replace("GestorPreAutorizacion.exe", "Setup.ini")
        ArchivoDatosGestor = Assembly.GetExecutingAssembly.Location.Replace("GestorPreAutorizacion.exe", "GestorPreAutorizacion.ini")

        mUserDB = LeerINI(ArchivoDatosGestor, "Server", "Srv_Remote_Login", "SA")

        mPwdDB = LeerINI(ArchivoDatosGestor, "Server", "Srv_Remote_Password", String.Empty)

        If Not (String.Equals(mUserDB, "SA", StringComparison.OrdinalIgnoreCase) And mPwdDB.Length = 0) Then

            mClsTmp = SafeCreateObject("SQLSafeGuard.Service")

            If (Not mClsTmp Is Nothing) Then
                mUserDB = mClsTmp.Decode(gCodProducto, gNombreProducto, gPK, mUserDB)
                mPwdDB = mClsTmp.Decode(gCodProducto, gNombreProducto, gPK, mPwdDB)
                If mUserDB Is Nothing Then mUserDB = String.Empty
                If mPwdDB Is Nothing Then mPwdDB = String.Empty
            Else
                MsgBox("Error de autenticación. Debe instalar y registrar el componente DLL SQLSafeGuard.")
                System.Environment.Exit(0)
            End If

        End If

LeerPKs:

        mPayPBKey = LeerINI(ArchivoDatosGestor, "Server", "PBK_APP", String.Empty)

        mPayPVKey = LeerINI(ArchivoDatosGestor, "Server", "PVK_APP", String.Empty)

        If Not (mPayPBKey.Length = 0 And mPayPVKey.Length = 0) Then

            mClsTmp = SafeCreateObject("SQLSafeGuard.Service")

            If (Not mClsTmp Is Nothing) Then
                mPayPBKey = mClsTmp.Decode(gCodProducto, gNombreProducto, gPK, mPayPBKey)
                mPayPVKey = mClsTmp.Decode(gCodProducto, gNombreProducto, gPK, mPayPVKey)
            Else
                MsgBox("Error de autenticación. Debe instalar y registrar el componente DLL SQLSafeGuard.")
                System.Environment.Exit(0)
            End If

        Else

            mClsTmp = SafeCreateObject("SQLSafeGuard.Service")

            If (mClsTmp Is Nothing) Then
                MsgBox("Error de autenticación. Debe instalar y registrar el componente DLL SQLSafeGuard.")
                System.Environment.Exit(0)
            End If

            MsgBox("Los datos de comunicación con la entidad bancaria no están definidos. Se le solicitarán a continuación. Indique Llave Publica en el Primer Campo y Llave Privada en el Segundo Campo.")

            Dim TmpVar As Object = mClsTmp.RequestAccess(gCodProducto, gNombreProducto, gPK)

            If TmpVar.Length > 0 Then
                'mPayPBKey = TmpVar(0) : mPayPVKey = TmpVar(1)
                EscribirIni(ArchivoDatosGestor, "Server", "PBK_APP", TmpVar(2))
                EscribirIni(ArchivoDatosGestor, "Server", "PVK_APP", TmpVar(3))
                GoTo LeerPKs
            End If

        End If

    End Sub

    Public Sub ConectarSql1()

Retry:

        Try

            'RUTACONEXIONSQL = "Integrated Security=SSPI;Persist Security Info=False;User ID=sa;Initial Catalog=VAD10;Data Source=" & RUTACONEXIONSQL

            RUTACONEXIONSQL = "Server=" & ServerInstance & "; Database=VAD10;User ID=" & mUserDB & ";PWD=" & mPwdDB & ";Trusted_Connection=False"

            falla = 0

            ' RUTACONEXIONSQL = csSql1


            ConexionSql1 = New SqlConnection
            ConexionSql1.ConnectionString = RUTACONEXIONSQL
            ConexionSql1.Open()

        Catch SQLAny As SqlException

            If (SQLAny.Number = 18456 And SQLAny.ErrorCode = -2146232060) Then

                Dim mClsTmp As Object = SafeCreateObject("SQLSafeGuard.Service")

                If (mClsTmp Is Nothing) Then
                    MsgBox("Error de autenticación. Debe instalar y registrar el componente DLL SQLSafeGuard.")
                    System.Environment.Exit(0)
                End If

                MsgBox("Los datos de acceso al servidor de base de datos no están definidos o son incorrectos. Se le solicitarán a continuación.")

                Dim TmpVar As Object = mClsTmp.RequestAccess(gCodProducto, gNombreProducto, gPK)

                If TmpVar.Length > 0 Then
                    mUserDB = TmpVar(0) : mPwdDB = TmpVar(1)
                    EscribirIni(ArchivoDatosGestor, "Server", "Srv_Remote_Login", TmpVar(2))
                    EscribirIni(ArchivoDatosGestor, "Server", "Srv_Remote_Password", TmpVar(3))
                    GoTo Retry
                End If

            End If

            falla = 1

        Catch ex As Exception

            falla = 1

        End Try

    End Sub

    Public Sub ConectarSql2()
        Try
            RUTACONEXIONSQL = "Integrated Security=SSPI;Persist Security Info=False;Initial Catalog=VAD10;Data Source=" & RUTACONEXIONSQL
            '            RUTACONEXIONSQL = csSql2
            ConexionSql2.ConnectionString = RUTACONEXIONSQL
            ConexionSql2.Open()
        Catch ex As Exception
            If falla = 0 Then falla = 1
        End Try
    End Sub

    Public Sub DesconectarSql1()
        If ConexionSql1.State = ConnectionState.Open Then
            Try
                ConexionSql1.Close()
                ConexionSql1.Dispose()
            Catch ex As Exception
                falla = 1
                ' MsgBox(ex.Message, MsgBoxStyle.Critical)
            End Try
        End If
    End Sub

    Public Sub DesconectarSql2()
        If ConexionSql2.State = ConnectionState.Open Then
            Try
                ConexionSql2.Close()
                ConexionSql2.Dispose()
            Catch ex As Exception
                falla = 1
                '                MsgBox(ex.Message, MsgBoxStyle.Critical)
            End Try
        End If
    End Sub

    Public Function consultarSql1(ByVal sql1 As String) As SqlDataReader
        Dim regsql1 As SqlClient.SqlDataReader = Nothing 'Guardara las filas de la consulta
        Try
            Dim querysql1 As New SqlClient.SqlCommand(sql1, ConexionSql1) 'Especificamos la sentencia SQL
            regsql1 = querysql1.ExecuteReader() 'Ejecuta la sentencia SQL
        Catch ex As Exception
            falla = 1
            '            MsgBox(ex.Message) 'Muestra un mensaje de Error si lo hay
        End Try
        Return regsql1
    End Function

    Public Function consultarSql2(ByVal sql2 As String) As SqlDataReader
        Dim regsql2 As SqlClient.SqlDataReader = Nothing 'Guardara las filas de la consulta
        Try
            Dim querysql2 As New SqlClient.SqlCommand(sql2, ConexionSql2) 'Especificamos la sentencia SQL
            regsql2 = querysql2.ExecuteReader() 'Ejecuta la sentencia SQL
        Catch ex As Exception
            falla = 1
            '            MsgBox(ex.Message) 'Muestra un mensaje de Error si lo hay
        End Try
        Return regsql2
    End Function

End Module
