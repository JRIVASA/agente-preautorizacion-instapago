﻿Imports System.Net
Imports System.Text

Module Pagos

    Public KeyId, PublicKeyId, NumeroPedido, NotaEntrega, MontoFinal, TipoPago, Amount, Description, CardHolder, CardHolderID, CardNumber, CVC, ExpirationDate, StatusId, IP, Id, TipoTransaccion,
           falla, ServerInstance, RUTACONEXIONSQL As String
    Public OrdenWeb, MontoOriginal, Transaccion_Referencia, Estatus, DETALLETRANSACCION As String
    Public OrderNumber, Address, City, ZipCode, State As String
    Public DebugMode As Boolean
    '   ACA SE GUARDAN LOS PARAMETROS DEL JSON
    Public SUCCESS, MESSAGE, CODE, REFERENCE, VOUCHER, SEQUENCE, APPROVAL, MONTO, FECHA, RESPONSECODE As String

    'Funciones de la API de Windows

    Private Declare Function GetPrivateProfileStringKey Lib "kernel32" Alias "GetPrivateProfileStringA" (ByVal lpApplicationName As String, ByVal lpKeyName As String, ByVal lpDefault As String, ByVal lpReturnedString As String, ByVal nSize As Integer, ByVal lpFileName As String) As Integer
    Private Declare Function GetPrivateProfileStringNullKey Lib "kernel32" Alias "GetPrivateProfileStringA" (ByVal lpApplicationName As String, ByVal lpKeyName As Integer, ByVal lpDefault As String, ByVal lpReturnedString As String, ByVal nSize As Integer, ByVal lpFileName As String) As Integer
    Private Declare Function WritePrivateProfileString Lib "Kernel32" Alias "WritePrivateProfileStringA" (ByVal lpApplicationName As String, ByVal lpKeyName As String, ByVal lpString As String, ByVal lpFileName As String) As Integer
    Private Declare Function WritePrivateProfileStringNullKey Lib "Kernel32" Alias "WritePrivateProfileStringA" (ByVal lpApplicationName As String, ByVal lpKeyName As Object, ByVal lpString As Object, ByVal lpFileName As String) As Integer

    Public Function LeerINI(ByVal sINIFile As String, ByVal sSection As String, ByVal sKey As String, ByVal sDefault As String) As String
        Dim sTemp As String '* 256
        Dim nLength As Integer
        sTemp = Space$(10000)
        nLength = GetPrivateProfileStringKey(sSection, sKey, sDefault, sTemp, 9999, sINIFile)
        LeerINI = Left$(sTemp, nLength)
    End Function

    Public Sub EscribirIni(ByVal SIniFile As String, ByVal SSection As String, ByVal sKey As String, ByVal sData As String)
        Try
            WritePrivateProfileString(SSection, sKey, sData, SIniFile)
        Catch Any As Exception
            Console.WriteLine(Any.Message)
        End Try
    End Sub

    Public Function CompletarPago(ByVal Id As String, ByVal Amount As String) As Boolean

        Try

            Dim Response As HttpWebResponse
            Dim ResponseContent As String
            Dim postdata As String
            Dim s As HttpWebRequest
            Dim enc As UTF8Encoding
            Dim postdatabytes As Byte()
            enc = New System.Text.UTF8Encoding()

            s = HttpWebRequest.Create("https://api.instapago.com/complete")
            postdata = "KeyId=" & KeyId & "&PublicKeyId=" & PublicKeyId & "&Id=" & Id & "&Amount=" & Amount

            postdatabytes = enc.GetBytes(postdata.Trim)
            s.Method = "POST"
            s.ContentType = "application/x-www-form-urlencoded"
            s.ContentLength = postdatabytes.Length

            Using stream = s.GetRequestStream()
                stream.Write(postdatabytes, 0, postdatabytes.Length)
            End Using
            '            Dim result = s.GetResponse()

            ' ACA SE OBTIENE LA RESPUESTA
            Response = s.GetResponse()

            ' ACA SE EXTRAE EL CONTENIDO DE LA RESPUESTA
            Using Reader As New System.IO.StreamReader(Response.GetResponseStream)
                ResponseContent = Reader.ReadToEnd
                Response.Close()
            End Using

            DETALLETRANSACCION = ResponseContent

            If DebugMode = True Then MsgBox(ResponseContent, , "Detalle de Funcion Completar Pago")

            ' ACA SE PROCESA EL CONTENIDO PARA DEVOLVER VALORES TRUE O FALSE
            Dim condicionTrue As Boolean = ResponseContent.Contains("success" & Chr(34) & ":true")
            Dim condicion201 As Boolean = ResponseContent.Contains("code" & Chr(34) & ":" & Chr(34) & "201")
            Dim condicionCerrada As Boolean = ResponseContent.Contains("message" & Chr(34) & ":" & Chr(34) & "Cerrada")
            Dim condicionEjecutada As Boolean = ResponseContent.ToUpper.Contains(("message" & Chr(34) & ":" & Chr(34) & "Completada").ToUpper) _
            Or ResponseContent.ToUpper.Contains(("message" & Chr(34) & ":" & Chr(34) & "Ejecutada").ToUpper)
            Dim condicionPreautorizada As Boolean = ResponseContent.Contains("message" & Chr(34) & ":" & Chr(34) & "Pre-autorizada")

            If DebugMode = True Then MsgBox("Condiciòn True= " & condicionTrue.ToString & "Condiciòn 201= " & condicion201.ToString & "Condiciòn Cerrada = " & condicionCerrada.ToString & "Condiciòn Preautorizada= " & condicionPreautorizada.ToString & " Condición Ejecutada= " & condicionEjecutada.ToString)

            If condicion201 = True And condicionTrue = True Then
                Return True
            Else
                Return False
            End If

        Catch ex As Exception
            MsgBox(ex.Message, , "CompletarPago")
            CompletarPago = False
        End Try

    End Function

    Public Function ConsultarActualizar(ByVal KeyId As String, ByVal PublicKeyId As String, ByVal Id As String) As Boolean

        Try

            Dim ResponseContent As String
            Dim getdata As String
            Dim s As HttpWebRequest

            If DebugMode = True Then MsgBox("Iniciando Consultar y Actualizar")

            getdata = "KeyId=" & KeyId & "&PublicKeyId=" & PublicKeyId & "&Id=" & Id

            s = HttpWebRequest.Create("https://api.InstaPago.com/payment?" + getdata.Trim)

            s.Method = "GET"
            Dim result = s.GetResponse()

            ' Aca se obtiene el contenido de la respuesta
            Using Reader As New System.IO.StreamReader(s.GetResponse().GetResponseStream)
                ResponseContent = Reader.ReadToEnd
                result.Close()
            End Using

            DETALLETRANSACCION = ResponseContent

            ' ACA SE PROCESA EL CONTENIDO PARA DEVOLVER VALORES TRUE O FALSE
            Dim condicionTrue As Boolean = ResponseContent.ToUpper.Contains(("success" & Chr(34) & ":true").ToUpper)
            Dim condicion201 As Boolean = ResponseContent.ToUpper.Contains(("code" & Chr(34) & ":" & Chr(34) & "201").ToUpper)
            Dim condicionCerrada As Boolean = ResponseContent.ToUpper.Contains(("message" & Chr(34) & ":" & Chr(34) & "Cerrada").ToUpper)
            Dim condicionEjecutada As Boolean = ResponseContent.ToUpper.Contains(("message" & Chr(34) & ":" & Chr(34) & "Completada").ToUpper) _
            Or ResponseContent.ToUpper.Contains(("message" & Chr(34) & ":" & Chr(34) & "Ejecutada").ToUpper)
            Dim condicionPreautorizada As Boolean = ResponseContent.ToUpper.Contains(("message" & Chr(34) & ":" & Chr(34) & "Pre-autorizada").ToUpper)

            If DebugMode = True Then MsgBox(ResponseContent.ToString, , "Detalle Consultar Pago")
            If DebugMode = True Then MsgBox("Condiciòn True= " & condicionTrue.ToString & " Condiciòn 201= " & condicion201.ToString & " Condiciòn Cerrada = " & condicionCerrada.ToString & " Condiciòn Preautorizada= " & condicionPreautorizada.ToString & " Condición Ejecutada= " & condicionEjecutada.ToString)

            '   ACA SE PROCEDE A TOMAR DECISIONES

            If condicionTrue = True And condicion201 = True And condicionCerrada = True Then

                If DebugMode = True Then MsgBox("Esta cerrada. ActualizarDetallePedido a 1")

                '   SI SE CUMPLE TODO ESTO SE ACTUALIZA EL REGISTRO A ESTATUS 1
                ActualizarDetallePedido(NumeroPedido, "1")

                ConsultarActualizar = False

            ElseIf condicionTrue = True And condicion201 = True And condicionEjecutada = True Then

                If DebugMode = True Then MsgBox("Ya fue ejecutada. ActualizarDetallePedido a 2")

                '   ACA SI SE CUMPLIO SE PONE EL ESTATUS EN 2
                ActualizarDetallePedido(NumeroPedido, "2")

                ConsultarActualizar = True

            Else

                '   SI NO SE CUMPLE ESTO SE VERIFICA SI NO ES PREAUTORIZADA
                If condicionPreautorizada = True Then

                    If DebugMode = True Then MsgBox("Completando pago")

                    '   ACA SI ES PREAUTORIZADA SE TRATA DE COMPLETAR
                    If CompletarPago(Id, MontoFinal) = True Then

                        If DebugMode = True Then MsgBox("Logro completar el pago.")

                        '   ACA SI SE CUMPLIO SE PONE EL ESTATUS EN 2
                        ActualizarDetallePedido(NumeroPedido, "2")

                        ConsultarActualizar = True

                    Else

                        If DebugMode = True Then MsgBox("No logro completar el pago.")

                        '   ACA SINO SE PUDO MALA SUERTE SE PONE EN ESTATUS 1
                        ActualizarDetallePedido(NumeroPedido, "1")

                        ConsultarActualizar = False

                    End If

                End If

            End If

        Catch ex As Exception
            System.Environment.Exit(0)
            ConsultarActualizar = False
        End Try

        Return ConsultarActualizar

    End Function

End Module
