﻿Imports System.Data.SqlClient
Imports Newtonsoft.Json

Module Parametros

    Public Const gCodProducto = 859
    Public Const gNombreProducto = "Stellar BUSINESS"
    Public gPK As String

    ' Advertencia: NO Cambiar ninguno de los 3 parámetros anteriores. Para efectos de resguardo de las 
    ' credenciales de acceso a SQL se esta utilizando la misma combinación de parámetros de Stellar BUSINESS. 
    ' Para que las claves encriptadas sean las mismas en todos los Agentes de Sincronización Web. 
    ' Despues que las claves hayan sido generadas si esto se llegara a cambiar no funcionaría la conexión SQL 
    ' Al desplegar una nueva versión. 

    Public mUserDB As String
    Public mPwdDB As String

    Public mPayPVKey As String
    Public mPayPBKey As String

    Public Archivo As String
    Public ArchivoDatosGestor As String

    Public Function LeeParametros()

        ' SI ES SOLO CONSULTA, NECESITARA SOLO 1 VALOR PERO NECESITA 3 PARAMETROS
        If My.Application.CommandLineArgs.Count > 0 Then

            ' ACA RECIBE EL NUMERO DE DOCUMENTO Y TIPO DE TRANSACCION: 1 Consultar Pago, 2 Crear Pago, 3 Completar Pago
            TipoTransaccion = My.Application.CommandLineArgs(0).Trim.ToUpper

            Select Case TipoTransaccion
                Case "1"
                    TipoTransaccion = "CONSULTAR Y ACTUALIZAR"
                Case "2"
                    TipoTransaccion = "CREAR PAGO"
                Case "3"
                    TipoTransaccion = "COMPLETAR PAGO"
            End Select

        End If

        If DebugMode = True Then MsgBox("Tipo Transaccion: " & TipoTransaccion)

        ''   ACA SI ES COMPLETAR PAGO, NECESITARA DOS PARAMETROS MAS QUE SON ESTATUS Y MONTO, APARTE DE LOS ANTERIORES
        'If My.Application.CommandLineArgs.Count = 5 Then

        '    '   ACA TIPO DE PAGO PARA EL CASO DE 1 ES UNA PRE AUTORIZACION EN EL CASO DE 2 ES UN PAGO COMPLETO
        '    StatusId = My.Application.CommandLineArgs(3).Trim

        '    '   ACA ENVIO LOS VALORES MAS IMPORTANTES PARA COMPLETAR EL PAGO
        '    Amount = My.Application.CommandLineArgs(4).Trim

        'End If

        '   ACA SI ES PARA CREAR UN PAGO SEA CUAL SEA, PIDE TODOS ESTOS ADEMAS. OJO: ESTA CANTIDAD NO CUENTA LOS OPCIONALES!!!!!!!!!!!!!!!!!!!!!!!!!
        'If My.Application.CommandLineArgs.Count = 12 Then

        '    '   ACA ENVIO LOS VALORES PARA REALIZAR UN PAGO. ESTOS SON DE LA TARJETA. LA FECHA DE LA TARJETA ENVIAR EN FORMATO MM/YYYY EL MONTO FORMATO 1.00
        '    CardNumber = My.Application.CommandLineArgs(5).Trim
        '    CVC = My.Application.CommandLineArgs(6).Trim
        '    ExpirationDate = My.Application.CommandLineArgs(7).ToString.Replace("/", "%2F")

        '    '   ACA ENVIO LOS VALORES PARA REALIZAR UN PAGO. ESTOS SON LOS DATOS PERSONALES: NOMBRE, CEDULA
        '    CardHolder = My.Application.CommandLineArgs(8).Trim.ToUpper
        '    CardHolderID = My.Application.CommandLineArgs(9).Trim.ToUpper

        '    '   ACA VALORES TECNICOS
        '    Description = My.Application.CommandLineArgs(10).Trim.ToUpper
        '    IP = My.Application.CommandLineArgs(11).Trim.ToUpper

        '    '   ACA VALORES OPCIONALES, NO USADOS HASTA AHORA
        '    'OrderNumber = My.Application.CommandLineArgs(12).Trim.ToUpper
        '    'Address = My.Application.CommandLineArgs(13).Trim.ToUpper
        '    'City = My.Application.CommandLineArgs(14).Trim.ToUpper
        '    'ZipCode = My.Application.CommandLineArgs(15).Trim.ToUpper
        '    'State = My.Application.CommandLineArgs(16).Trim.ToUpper

        'End If

        Return True

    End Function

    Public Sub ConsultarDetallePedido(ByVal NumeroPedido As String)

        Try

            If falla = 0 Then

                Dim query As String = "SELECT OrdenWeb, MontoOriginal, Transaccion_Referencia, Transaccion_Id, Estatus FROM MA_VENTAS_PREAUTORIZACION_WEB WHERE (Pedido = '" & NumeroPedido & "')"

                Dim connection As New SqlConnection(RUTACONEXIONSQL)
                Dim command As New SqlCommand(query, connection)
                connection.Open()
                Dim registrosSql1 As SqlDataReader = command.ExecuteReader()

                '                registrosSql1 = consultarSql1(query)

                If registrosSql1.HasRows = True Then
                    While registrosSql1.Read
                        OrdenWeb = registrosSql1("OrdenWeb").ToString.Trim
                        MontoOriginal = registrosSql1("MontoOriginal").ToString.Trim
                        Id = registrosSql1("Transaccion_Id").ToString.Trim
                        Transaccion_Referencia = registrosSql1("Transaccion_Referencia").ToString.Trim
                        Estatus = registrosSql1("Estatus").ToString.Trim
                    End While
                End If
                registrosSql1.Close()

                If DebugMode = True Then
                    MsgBox("OrdenWeb: " & OrdenWeb)
                    MsgBox("MontoOriginal: " & MontoOriginal)
                    MsgBox("Id: " & Id)
                    MsgBox("Transaccion Referencia: " & Transaccion_Referencia)
                    MsgBox("Estatus: " & Estatus)
                End If
            End If
        Catch ex As Exception
            MsgBox(ex.Message, , "Consultar Detalle Pedido")
        End Try
    End Sub

    Public Sub ActualizarDetallePedido(ByVal NumeroPedido As String, ByVal ValorEstatus As String)

        Try
            If falla = 0 Then
                Dim query As String = "UPDATE MA_VENTAS_PREAUTORIZACION_WEB Set Estatus = '" & ValorEstatus & "', NotaDeEntrega = '" & NotaEntrega & "', MontoFinal ='" & MontoFinal & "' Where Pedido = '" & NumeroPedido & "'"

                Dim connection As New SqlConnection(RUTACONEXIONSQL)
                Dim command As New SqlCommand(query, connection)
                connection.Open()
                Dim registrosSql1 As SqlDataReader = command.ExecuteReader()

                '                registrosSql1 = consultarSql1(query)
                registrosSql1.Close()
            End If
        Catch ex As Exception
            MsgBox(ex.Message, , "Actualizar Detalle Pedido")
        End Try

    End Sub

    Public Sub ENRUTAR()

        Try
            If falla = 0 Then

                Dim query As String = "SELECT CS_Servidor FROM Ma_organizacion WHERE CS_Organizacion = 'Prebo'"

                If DebugMode = True Then MsgBox(query)

                Dim connection As New SqlConnection(RUTACONEXIONSQL)
                Dim command As New SqlCommand(query, connection)
                connection.Open()
                Dim registrosSql1 As SqlDataReader = command.ExecuteReader()
                '                registrosSql1 = consultarSql1(query)

                If registrosSql1.HasRows = True Then
                    While registrosSql1.Read
                        RUTACONEXIONSQL = registrosSql1("Cs_Servidor").ToString.Trim
                    End While
                End If
                registrosSql1.Close()

                If DebugMode = True Then MsgBox(RUTACONEXIONSQL)

            End If
        Catch ex As Exception
            MsgBox(ex.Message, "Enrutar")
        End Try
    End Sub

    Public Function GuardarRegistroTransaccion() As Boolean

        '   ACA PRIMERO VERIFICO SI TABLA EXISTE O NO. SI NO EXISTE LA CREO
        If Not ExisteTabla("MA_SOLICITUD_INSTAPAGO") Then
            CrearTabla("MA_SOLICITUD_INSTAPAGO")
        End If

        '   ACA HAGO EL PROCESO DE GUARDADO DEL REGISTRO

        Try
            If falla = 0 Then

                Dim query As String = "INSERT INTO MA_SOLICITUD_INSTAPAGO (OrdenWeb, Success, Message, Code, ResponseCode, Reference, Sequence, Approval, MontoFinal, Fecha) VALUES ('" & NumeroPedido & "','" & SUCCESS & "','" & MESSAGE & "','" & CODE & "','" & RESPONSECODE & "','" & REFERENCE & "','" & SEQUENCE & "','" & APPROVAL & "','" & MONTO & "', GetDate())"

                Dim connection As New SqlConnection(RUTACONEXIONSQL)
                Dim command As New SqlCommand(query, connection)
                connection.Open()
                Dim registrosSql1 As SqlDataReader = command.ExecuteReader()

                '                registrosSql1 = consultarSql1(query)
                registrosSql1.Close()

                Return True

            End If
        Catch ex As Exception
            MsgBox(Err.Description, , "Guardar Log")
        End Try

        Return False

    End Function

    Public Function ExisteTabla(ByVal NombreTabla As String) As Boolean
        Try

            Dim Cantidad As Integer
            Dim query As String = "SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = '" & NombreTabla & "'"

            Dim connection As New SqlConnection(RUTACONEXIONSQL)
            Dim command As New SqlCommand(query, connection)
            connection.Open()
            Dim registrosSql1 As SqlDataReader = command.ExecuteReader()
            '                registrosSql1 = consultarSql1(query)

            Cantidad = 0
            '                cantidad = CInt(command.ExecuteScalar())

            If registrosSql1.HasRows = True Then
                While registrosSql1.Read
                    Cantidad = 1
                End While
            End If
            registrosSql1.Close()

            If Cantidad = 0 Then
                ExisteTabla = False
            Else
                ExisteTabla = True
            End If

        Catch ex As Exception
            MsgBox(ex.Message, , "Enrutar")
            End
        End Try
    End Function

    Public Sub CrearTabla(ByVal NombreTabla As String)
        Try
            If falla = 0 Then

                Dim query As String = "CREATE TABLE " & NombreTabla & " (OrdenWeb NVARCHAR(50), Success NVARCHAR(20), Message NVARCHAR(500), Code NVARCHAR(20), ResponseCode NVARCHAR(10), Reference NVARCHAR(50), Sequence NVARCHAR(50), Approval NVARCHAR(50), MontoFinal NVARCHAR(20), Fecha DATETIME)"

                Dim connection As New SqlConnection(RUTACONEXIONSQL)
                Dim command As New SqlCommand(query, connection)
                connection.Open()
                Dim registrosSql1 As SqlDataReader = command.ExecuteReader()

                '                registrosSql1 = consultarSql1(query)
                registrosSql1.Close()

            End If
        Catch ex As Exception
            MsgBox(ex.Message, , "Crear Tabla")
            End
        End Try
    End Sub

    Public Sub LeerJSON(ByVal Contenido As String)

        Dim tempPost = New With {Key .SUCCESS = ""}
        Dim post = JsonConvert.DeserializeAnonymousType(Contenido, tempPost)

        If Not String.IsNullOrEmpty(post.SUCCESS) Then
            SUCCESS = post.SUCCESS.ToUpper
        Else
            SUCCESS = "FALSE"
        End If

        'If UCase(SUCCESS) = UCase("TRUE") Then

        Dim tempPost1 = New With {Key .MESSAGE = ""}
        Dim post1 = JsonConvert.DeserializeAnonymousType(Contenido, tempPost1)

        If Not String.IsNullOrEmpty(post1.MESSAGE) Then
            MESSAGE = post1.MESSAGE.ToUpper.Replace("'", """")
        Else
            MESSAGE = ""
        End If

        Dim tempPost2 = New With {Key .CODE = ""}
        Dim post2 = JsonConvert.DeserializeAnonymousType(Contenido, tempPost2)

        If Not String.IsNullOrEmpty(post2.CODE) Then
            CODE = post2.CODE.ToUpper
        Else
            CODE = ""
        End If

        Dim tempPost3 = New With {Key .REFERENCE = ""}
        Dim post3 = JsonConvert.DeserializeAnonymousType(Contenido, tempPost3)

        If Not String.IsNullOrEmpty(post3.REFERENCE) Then
            REFERENCE = post3.REFERENCE.ToUpper
        Else
            REFERENCE = ""
        End If

        Dim tempPost4 = New With {Key .VOUCHER = ""}
        Dim post4 = JsonConvert.DeserializeAnonymousType(Contenido, tempPost4)

        If Not String.IsNullOrEmpty(post4.VOUCHER) Then
            VOUCHER = post4.VOUCHER.ToUpper
        Else
            VOUCHER = ""
        End If

        Dim tempPost5 = New With {Key .SEQUENCE = ""}
        Dim post5 = JsonConvert.DeserializeAnonymousType(Contenido, tempPost5)

        If Not String.IsNullOrEmpty(post5.SEQUENCE) Then
            SEQUENCE = post5.SEQUENCE.ToUpper
        Else
            SEQUENCE = ""
        End If

        Dim tempPost6 = New With {Key .APPROVAL = ""}
        Dim post6 = JsonConvert.DeserializeAnonymousType(Contenido, tempPost6)

        If Not String.IsNullOrEmpty(post6.APPROVAL) Then
            APPROVAL = post6.APPROVAL.ToUpper
        Else
            APPROVAL = ""
        End If

        Dim tempPost7 = New With {Key .amount = ""}
        Dim post7 = JsonConvert.DeserializeAnonymousType(Contenido, tempPost7)

        If Not String.IsNullOrEmpty(post7.amount) Then
            MONTO = post7.amount.ToUpper
        Else
            MONTO = ""
        End If

        Dim tempPost8 = New With {Key .RESPONSECODE = ""}
        Dim post8 = JsonConvert.DeserializeAnonymousType(Contenido, tempPost8)

        If Not String.IsNullOrEmpty(post8.RESPONSECODE) Then
            RESPONSECODE = post8.RESPONSECODE.ToUpper
        Else
            RESPONSECODE = ""
        End If

        FECHA = Date.Now

        'End If

    End Sub

    Public Function SafeCreateObject(pClass As String, Optional pServerName As String = "") As Object

        Try

            If Trim(pServerName) = "" Then
                SafeCreateObject = CreateObject(pClass)
            Else
                SafeCreateObject = CreateObject(pClass, pServerName)
            End If

            Exit Function

        Catch Any As Exception

            Console.WriteLine(Any.Message)

            SafeCreateObject = Nothing

        End Try

    End Function

End Module